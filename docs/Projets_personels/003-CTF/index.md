---
tags:
    - Cyberdéfense
---

# CTFs

Voici quelques résultats de CTF auquel j'ai participé, dans l'ordre chronologique

## Online

### Midnight Flag - Infektion

- Equipe : Masterhaxxor
- Membres : 2
- Rang : 154èmes
- Points : 517
- Flags : Meduzom I + Trailer

### NoBrackets. Edition 2022

- Equipe : Les galériens
- Membres : 2+2 personnes inconues
- Rang : 10ème Jour 1

### Midnight Flag - Black Box

- Equipe : MasterHaxxor
- Membres : 2
- Rang : 123 sur 358
- Points : 300
- Flags : Welcome, DNStonks, Time Traveler 1, Dernier vol, Mysterious Passenger 1, Xd33r 1

### Six'CTF

- Equipe : Feur
- Membres : 3
- Rang: 1er

### Midnight Flag - Backslash

- Equipe : XOR TEAM
- Membres : 5
- Rang : 36 sur 158
- Points : 4586

### CTF InterIUT 2024

- Equipe : XOR TEAM
- Membres : 4
- Rang : 13/20
- Points : 3537

### Six'CTF 2024

- Equipe : TEAM XOR(2)
- Membres : 3
- Rang: 3ème
- Points: 13277

## Futurs

### CTF Hack'Lantique.

- Equipe : TBA
- Membres : TBA
- Rang: TBA
- Points: TBA


## Platformes d'entrainement

### TryHackMe

- Niveau : 7
- Points : 3254
- Profil : [https://tryhackme.com/p/UnityXotak](https://tryhackme.com/p/UnityXotak)

### Root-Me

- Points : 355
- Profil : [https://www.root-me.org/loulou310-417929](https://www.root-me.org/loulou310-417929)
