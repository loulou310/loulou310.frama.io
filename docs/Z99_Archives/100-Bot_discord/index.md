---
tags :
    - Node.JS
    - JavaScript
    - Application
---

# Bot Discord

Mon but initial avec ce bot, était de le réaliser pour moi et mes amis. Le projet a évolué pour devenir un projet permettant à n'importe qui de déployer un bot d'administration facilement.

Il suffit seulement de créer un fichier de configuration et d'installer les modules node nécessaires, comme mentionné dans le readme.

## Fonctionalitées

Le bot utilise la fonctionnalité majeure de la version 10 de l'API de Discord : les "Slash commands".
Ces commandes sont plus intuitives à utiliser car leurs syntaxes sont détaillées lors de l'écriture de celles-ci, contrairement aux anciennes commandes dont il fallait quasiment apprendre la syntaxes si on ne voulait pas lire la documentation du bot à chaque fois que l'on veut l'utiliser


<img src="../../images/Encart discord.png" alt="Encart des 'Slash commands' de discord" width="75%">

Les commandes sont définies dans le code du bot, dont l'affichage est géré par Discord après les avoir déployées.

<img src="..\..\images\Syntaxe.png" alt="Exemple d\'aide syntaxique. Le type d'argument a donner est défini dans le code de la commande" width="75%">

Voici l'aide syntaxique proposée par Discord. Le type d'argument à donner est défini dans le code de la commande

## Commandes

- Administration
    - Ban (Bannit un utilisateur du serveur)
    - Kick (Éjecte un utilisateur du serveur)
    - Timeout (Retire temporairement le droit d'envoyer des messages et de rejoindre un salon vocal d'un utilisateur)
- Informations utilisateurs/Serveur
    - User (Renvoie les informations d'un utilisateur)
    - Serveur (Renvoie les informations sur le serveur actuel)
    - Ping (Renvoie pong, ainsi que la latence du bot)
- Autres
    - Poll (Crée un sondage, auquel les utilisateurs peuvent répondre avec les réactions)
    - 8ball (Renvoie une prédiction à propos d'une action)
    - Meme (Renvoie un meme aléatoire récupéré depuis Reddit)

D'autres commandes et fonctionnalités arriveront bientôt, au fur et à mesure que j'effectue la migration depuis la version 8 de l'API

Les ressources que j'ai utilisées sont le guide officiel de la librairie Discord.js ainsi que sa documentation.  

## Code

Le code est constitué d'un code principal, contenu dans un fichier. Les commandes et les évènements sont stockés dans des fichiers séparés.

=== "Fichier principal"
 
    Le bot dispose donc d'un handler pour les commandes et les évènements, qui sont chargés a chaque démarage du bot
    
    ```js
    client.commands = new Collection();
    const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
    const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('.js'));

    for (const file of commandFiles) {
        const command = require(`./commands/${file}`);
        // Set a new item in the Collection
        // With the key as the command name and the value as the exported module
        client.commands.set(command.data.name, command);
    }

    for (const file of eventFiles) {
        const event = require(`./events/${file}`);
        if (event.once) {
            client.once(event.name, (...args) => event.execute(...args));
        } else {
            client.on(event.name, (...args) => event.execute(...args));
        }
    }
    ```

=== "Commande meme"

    La commande meme va chercher un meme dans un flux personnalisé Reddit, et récupère les informations grâce au JSON associé au flux

    ```js
    const meme = await fetch("https://www.reddit.com/user/loulou310/m/memes/top/.json?sort=top&ampt=day&amplimit=100")
    .then(res => res.json())
    .then(json => json.data.children)



    const img = meme[Math.floor(Math.random() * meme.length)].data
    const embed = new MessageEmbed()
    .setDescription(img.title)
    .setImage(img.url)
    .setFooter(`Powered by ${img.subreddit_name_prefixed}`)
    ```

=== "Commande ban"

    ```js
    const userToBan = interaction.options.getUser('target')
    const reason = (interaction.options.getString('reason') || "No reason specified")
    const days = interaction.options.getInteger('days')
    if (!userToBan) return
    userToBan ? interaction.guild.members.cache.get(userToBan.id).ban({days : days, reason: reason}) : interaction.channel.send("L'utilisateur n'existe pas");

    const embed = new MessageEmbed()
    .setAuthor(`${userToBan.tag} (${userToBan.id})`)
    .setColor("#ffa500")
    .setDescription(`**Action**: ban\n**Raison**: ${reason}\n**Durée**: ${days} jours`)
    .setThumbnail(userToBan.avatarURL())
    .setTimestamp()
    .setFooter(`${interaction.member.user.tag}`, interaction.member.user.displayAvatarURL());

    interaction.reply({ embeds: [embed] })
    ```

Lien du repository : <a href="https://github.com/loulou310/bot-framework">Lien</a>