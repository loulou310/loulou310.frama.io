---
tags:
    - Node.JS
    - Markdown
    - CI/CD
    - Web
---

# Site vitrine

Toujours dans le web, ma mère s'installe à son compte. Mon rôle dans tout ceci ? J'ai pour tâche de réaliser comme page web un site vitrine avec une fonctionalité de blog. Pour ce faire, j'ai décidé de ne pas utiliser mkdocs comme j'aurais pu le faire habituellement. J'utilise un tout autre outil. J'ai opté pour Docusaurus. Cela change énormément. La partie configuration du site est plus compliquée, et la pipeline utilise node et non python. Le repo est accessible [ici](https://framagit.org/AE56/AE56.frama.io). Le site est actuellement vide, mais est déja configuré, ainsi que le domaine.