---
tags :
     - C++
     - Jeu
     - CI/CD
---

# Jeu simple

Ce projet est un petit projet improvisé, dans le but d'appliquer les connaissances que j'ai acquises en C++, notamment la création de classes et de méthodes. C'est un jeu très simple, en mode texte ou 2 personnages s'affrontent. L'ennemi à des action prédéfinies en fonction des actions du joueur.

## Extraits de code

=== "Extrait de la boucle principale"
    
    Cet extrait est la partie du code qui gère l'attaque.

    ```cpp
    if(action == "attaque") {
            std::string typeAttaque;
            std::cout << "Quel type d'attaque voulez vous effectuer ?\nPour une attaque physique, ecrivez \"physique\". Pour une attaque magique, ecrivez \"magique\"" << std::endl;
            std::cin >> typeAttaque;
            while (typeAttaque != "physique" && typeAttaque != "magique")
            {
                std::cout << "Type d'attaque invalide, veuillez reessayer.\n";
                std::cin >> typeAttaque;
            }

            if (typeAttaque == "physique")
            {
                Personnage1.attaquer(Personnage2);
                Personnage2.attaquer(Personnage1);
            }
            else if (typeAttaque == "magique")
            {
                Personnage1.attaqueMagique(Personnage2);
                Personnage2.boirePotionDeMana(50);
            }
            
        }
    ```

=== "Classe Arme"

    Cet extrait nous montre une classe très simple, avec le constructeur par défaut, une méthode pour changer l'arme équipée par le personnage, et un accesseur .

    ```cpp
    Arme::Arme() : m_nom("Epee rouilée"), m_degat(10) {}
    
    Arme::Arme(std::string nom, int degat): m_nom(nom), m_degat(degat) {}
    
    void Arme::changer(std::string nom, int degat) 
    {
        m_nom = nom;
        m_degat = degat;
    }
    
    void Arme::afficher() const
    {
        std::cout << "Arme : " << m_nom << std::endl << "Degats : " << m_degat << std::endl; 
    }
    
    int Arme::getDegat() const
    {
        return m_degat;
    }
    ```

=== "Extrait de la classe Personnage"

    C'est dans cette classe que les différentes actions des personnages sont traitées. Une description des actions est aussi affichée afin que le joueur puisse suivre les actions de l'adversaire

    ```cpp
    void Personnage::attaqueMagique(Personnage &cible)
    {
        std::cout << getNom() << " utiliste un attaque magique sur " << cible.getNom() << std::endl;
        std::cout << cible.getNom() << " subit 30 points de degats" << std::endl;
        cible.recevoirDegat(30);
        m_mana -= 50;

        if(m_mana < 0) {
            m_mana = 0;
        }
    }

    void Personnage::boirePotionDeVie(int vieRestauree)
    {
        std::cout << getNom() << " boit une potion de vie et recupere " << vieRestauree << " points de vie\n";

        m_vie += vieRestauree;

        if (m_vie > 100)
        {
            m_vie = 100;
        }

    }

    bool Personnage::estVivant() const
    {
        return m_vie > 0;
    }

    void Personnage::afficherEtat()
    {
        if (estVivant())
        {
        std::cout << getNom() << " :" << std::endl;
        std::cout << "Statut : Vivant\n";
        std::cout << "Vie : " << m_vie << std::endl;
        std::cout << "Mana : " << m_mana << std::endl;
        m_arme.afficher();  
        }
        else
        {
            std::cout << getNom() << " :" << std::endl;
            std::cout << "Status : Mort" << std::endl;
        }

    }
    ```


Pour des raisons de facilité de lecture, seuls quelques extraits de code sont présentés. Pour plus de détails, veuillez accéder à [ce repo](https://framagit.org/loulou310/jeu-basique)

## Compétences tirées de ce projet

Grace à ce projet, j'ai tout d'abord créé une pipeline d'Intégration continue (CI). Pour simplifier la pipeline, j'ai créé également un fichier makefile. Enfin, j'ai utilisé le système de Releases de GitLab pour fournir des versions précompilées.

## Liens

Repo : [https://framagit.org/loulou310/jeu-basique](https://framagit.org/loulou310/jeu-basique)

Releases : [https://framagit.org/loulou310/jeu-basique/-/releases](https://framagit.org/loulou310/jeu-basique/-/releases)
