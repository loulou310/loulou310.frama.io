---
tags:
    - Linux
    - Systèmes d'exploitation
---

# Emerald OS

Emerald OS est une distribution Linux basée sur Arch Linux axée vers la Cyberdéfense. Pour l'instant, c'est juste une image disque d'Arch Linux, qui est livrée avec l'environnement de bureau KDE, Firefox et VSCodium. Le projet est encore en alpha, qui est très primitive, étant donné qu'il n'y a pas d'installateur, mis a part `arch-installer` livré par défaut avec l'image. Aucun iso n'est disponible pour le moment dues aux limitation de l'exécuteur partagé de framagit.

Le dépot est accessible [ici](https://framagit.org/xotak/emerald-os)

## Fonctionalités

- Archstrike : L'image inclut le dépot communautaire Archstrike, qui aglomère nombreux outils comme Burp Suite ou le framework Metasploit.
- Accès au AUR : L'image inclut yay, un outil en ligne de commande permettant d'accéder a l'Arch User Repository
- (Bientôt) Calamares : L'image inclut l'assistant d'installation Calamares, permetant d'installer Emerald OS facilement sur une machine.

## Captures d'écran : 

Bientôt