# 🏡 Accueil

Bienvenue.
Sur cette page vous trouverez différents projets en programmation, que j'ai réalisé personnellement ou en cours de NSI. Vous pouvez naviguer a travers ce site grâce a la barre de navigation en haut de la page.


## A propos de moi

Je m'appelle Jean-Louis Emeraud, j'ai 17 ans et je suis actuellement en deuxième année de BUT Informatique, parcours déploiment d'application communicantes sécurisées, à l'IUT de Vannes. Je participe également au "Parcours Renforcé" en partenariat avec l'ENSIBS, qui propose des cours d'introduction à la cyberdéfense. J'ai passé un bac général avec les spécialités Mathématiques et Numérique et Sciences Informatique (NSI), et l'option Mathématiques Expertes. Je souhaite intégrer la formation d'ingénieur en Cyberdéfense de l’ENSIBS

Je maitrise le Python, le Java et le développement web (HTML, CSS et JavaScript). J'ai aussi, à mes heures perdues, appris des bases en C++ orienté objet, en Rust et en Go. Je m'intéresse également a la cyberdéfense. Je m'entraine sur des platformes comme root-me ou TryHackMe.

J'ai également quelques compétences hors informatique. Mon père étant artisan couvreur, j'ai été amené a l'aider sur différents chantiers pendant les vacances scolaires. Je bricole aussi de temps en temps à la maison. Nous construisons également une résidence secondaire dans le Finistère, et jusqu'a présent j'ai participé à la maçonnerie. 

J'ai fait partie de l'équipe[^1] Backstage depuis 3 ans. Le but de cet équipe est de réaliser des installations matérielles, gérer les effets lumineux et le mixage son. Cette équipe nécessite une grande coordination entre les différents membres pour faciliter les installations et pour avoir une bonne synchronisation entre les différents postes.


Contenu mis à jour le `18/02/2025`  
Backend mis à jour le `10/12/2024`  (Màj/Pipelines/Configuration)


[^1]: Voir [ce lien](https://fr.m.wikipedia.org/wiki/Coll%C3%A8ge-lyc%C3%A9e-pr%C3%A9pa_Saint-Fran%C3%A7ois-Xavier_de_Vannes#Les_%C3%A9quipes_du_lyc%C3%A9e)
